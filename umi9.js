const make = require('./cli/make')

const [,, ...args] = process.argv;

const [commandName, ...rest] = args;

if(commandName.startsWith('make')) {
    make({
        command: commandName,
        args: rest
    })
}